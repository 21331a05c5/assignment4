import java.time.LocalTime;
import java.time.temporal.Temporal;
import java.time.LocalDate;
import java.time.LocalDateTime;
public class localtime {
    public static void main(String[] args) {
            // adjustInto(Temporal temporal)
                    LocalTime time1 = LocalTime.of(12, 30);
                            LocalTime time2 = LocalTime.of(15, 45);
                                    Temporal adjustedTime = time2.adjustInto(time1);
                                            System.out.println("Adjusted time1: " + adjustedTime);
                                                    LocalDate date = LocalDate.of(2023, 5, 15);
                                                            LocalTime time3 = LocalTime.of(10, 30);
                                                                    LocalDateTime dateTime = time3.atDate(date);
                                                                            System.out.println("Date and time: " + dateTime);
                                                                                    LocalTime time4 = LocalTime.of(18, 45);
                                                                                            int hour = time4.getHour();
                                                                                                    System.out.println("Hour: " + hour);
                                                                                                            LocalTime time5 = LocalTime.of(22, 15);
                                                                                                                    int minute = time5.getMinute();
                                                                                                                            System.out.println("Minute: " + minute);
                                                                                                                                }
                                                                                                                                }
